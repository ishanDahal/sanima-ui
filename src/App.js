import React from 'react';
import 'normalize.css';
import './assets/scss/main.scss';
import NewsAndEvents from './Component/NewsAndEvents';
import Footer from './Component/Footer';

function App() {
  return (
    <>
      <main>
        <NewsAndEvents />
      </main>
      <Footer/>
    </>
  );
}

export default App;
