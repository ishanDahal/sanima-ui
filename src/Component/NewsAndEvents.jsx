import React from 'react';
import pic1 from '../assets/image/News1.png';
import pic2 from '../assets/image/News2.png';
import pic3 from '../assets/image/News3.png';
import pic4 from '../assets/image/News4.png';
import eye from '../assets/image/Eye.png';
import arrow from '../assets/image/arrow.png';


function NewsAndEvents() {
    return (
        <section>
            <div className="heading">
                <h3>News And Events</h3>
                <div className="heading-view">
                    {/* <p>View All</p><span className="heading-image"><img src={arrow} alt="arrow"/></span> */}
                </div>
            </div>
            <div className="news-container">
                <div className="news-card">
                    <figure>
                        <img src={pic1} alt="image1" />
                        <figcaption>
                            <h5>IPO Issuance of NIC Asia Lag..</h5>
                            <p>Now you can pay your demat and meroshare fees online..</p>
                            <hr />
                            <div className="dateAndEye">
                                <div>
                                    <p>2019-Jun-02</p>
                                </div>
                                <div className="dateAndEye-img">
                                    <img src={eye} alt="eye" />
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div className="news-card">
                    <figure>
                        <img src={pic2} alt="image2" />
                        <figcaption>
                            <h5>IPO Issuance of NIC Asia Lag..</h5>
                            <p>Now you can pay your demat and meroshare fees online..</p>
                            <hr />
                            <div className="dateAndEye">
                                <div>
                                    <p>2019-Jun-02</p>
                                </div>
                                <div className="dateAndEye-img">
                                    <img src={eye} alt="eye" />
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div className="news-card">
                    <figure>
                        <img src={pic3} alt="image3" />
                        <figcaption>
                            <h5>IPO Issuance of NIC Asia Lag..</h5>
                            <p>Now you can pay your demat and meroshare fees online..</p>
                            <hr />
                            <div className="dateAndEye">
                                <div>
                                    <p>2019-Jun-02</p>
                                </div>
                                <div className="dateAndEye-img">
                                    <img src={eye} alt="eye" />
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div className="news-card">
                    <figure>
                        <img src={pic4} alt="image4" />
                        <figcaption>
                            <h5>IPO Issuance of NIC Asia Lag..</h5>
                            <p>Now you can pay your demat and meroshare fees online..</p>
                            <hr />
                            <div className="dateAndEye">
                                <div>
                                    <p>2019-Jun-02</p>
                                </div>
                                <div className="dateAndEye-img">
                                    <img src={eye} alt="eye" />
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </section>
    )
}

export default NewsAndEvents
