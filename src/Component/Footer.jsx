import React from 'react';
import sanima from '../assets/image/sanima.png';
import twitter from '../assets/image/twitter.png';
import fb from '../assets/image/fb.png';
import linkedin from '../assets/image/linkedin.png';

function Footer() {
    return (
        <footer>
            <div className="footer">
                <div className="footer-container">
                    <div className="footer-content">
                        <div className="content1">
                            <img src={sanima} alt="sanima" />
                            <p className="sanima-desc">Sanima Capital Ltd., a wholly-owned subsidiary of Sanima Bank Ltd., provides Merchant Banking and Investment Banking services as licensed by its apex regulator SEBON.</p>
                            <div className="sanima-connect">
                                <h5>Connect us with Social Media</h5>
                                <div className="sanima-social">
                                    <div className="social-sites">
                                        <img src={fb} alt="fb" />
                                    </div>
                                    <div className="social-sites">
                                        <img src={twitter} alt="twitter" />
                                    </div>
                                    <div className="social-sites">
                                        <img src={linkedin} alt="linkedin" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h4>Explore</h4>
                            <ul>
                                <li>About Us</li>
                                <li>Board of Directors</li>
                                <li>Management Team</li>
                            </ul>
                        </div>
                        <div>
                            <h4>Our Services</h4>
                            <ul>
                                <li>Issue Management</li>
                                <li>Securities Underwriting</li>
                                <li>Depository Participant</li>
                                <li>Registration Services</li>
                                <li>Portfolio Management Services</li>
                                <li>Advisory Service</li>
                                <li>Mutual Fund</li>

                            </ul>
                        </div>
                        <div>
                            <h4>Important Links</h4>
                            <ul>
                                <li>Nepal Rastra Bank</li>
                                <li>Sanima Bank Limited</li>
                                <li>Nepal Stock Exchange Limited</li>
                                <li>Securities Board of Nepal</li>
                                <li>CDS and Clearing Limited</li>
                                <li>Merchant Banking Association of Nepal</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="copyright">
                <p>Copyright © 2021 Sanima Capital Ltd. Maintained And Developed by<span style={{color:'#4EA774'}}> InfoDevelopers Pvt. Ltd.</span></p>
            </div>
        </footer>

    )
}

export default Footer
